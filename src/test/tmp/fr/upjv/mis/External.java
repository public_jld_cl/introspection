package fr.upjv.mis;



/**
 *
 * @author jldeleage
 */
public class External {

    public Internal getMyInternal() {
        return myInternal;
    }

    public void setMyInternal(Internal myInternal) {
        this.myInternal = myInternal;
    }

    public int getExternalValue() {
        return externalValue;
    }

    public void setExternalValue(int externalValue) {
        this.externalValue = externalValue;
    }

    public void add(int inValue) {
        setExternalValue(getExternalValue() + inValue);
    }


    public class Internal {

        public int getInternalValue() {
            return internalValue;
        }

        public void setInternalValue(int internalValue) {
            this.internalValue = internalValue;
        }

        public int getExternalValue() {
            return External.this.getExternalValue();
        }

        public void add(int inValue) {
            setInternalValue(getInternalValue() + inValue);
        }


        private  int  internalValue;
    }       // Internal


    private Internal    myInternal = new Internal();
    private int         externalValue;

}       // External

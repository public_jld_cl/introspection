package fr.upjv.mis;


/**
 *
 * @author jldeleage
 */
public class ExternalUser {
    

    public External addTo(External inExternal, int inValue) {
        External    result = new External();
        result.setExternalValue(inExternal.getExternalValue() + inValue);
        return result;
    }


    public External addTo(External.Internal inInternal, int inValue) {
        External    result = new External();
        result.setExternalValue(inInternal.getExternalValue());
        result.getMyInternal().setInternalValue(inInternal.getInternalValue() + inValue);
        return result;
    }



}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.upjv.mis;

/**
 *
 * @author jldeleage
 */
public class Multiplier {
    
    public Dumb2 multiply(Dumb2 inThis, int inHowMany) {
        Dumb2 result = new Dumb2();
        result.setValue(inThis.getValue() * inHowMany);
        return result;
    }


}

package fr.upjv.mis;



/**
 *
 * @author jldeleage
 */
public class Dumb2 {

    public Dumb2() {
    }

    public Dumb2(int inValue) {
        value = inValue;
    }

    public Dumb2 add(int inValue) {
        Dumb2 result = new Dumb2(value);
        result.value += inValue;
        return result;
    }


    public Dumb2 mult(int inValue) {
        Dumb2 result = new Dumb2(value * inValue);
        return result;
    }


    public int getValue() {
        return value;
    }

    public void setValue(int inValue) {
        value = inValue;
    }

    private     int     value;

}
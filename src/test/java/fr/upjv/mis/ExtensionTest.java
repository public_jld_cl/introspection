package fr.upjv.mis;


import java.net.MalformedURLException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author jldeleage
 */
public class ExtensionTest {
    
    public ExtensionTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }


    @Test
    public void extensionTest() throws NoSuchMethodException {
        System.out.println("extension");
        Object result = new ObjectIntrospector("hello World")
                .add(new StringExtension())
                .then("substring", 1, 4)
                .then("initialToUpperCase")
                .getMyThis();
        assertEquals("Ell", result);
    }

    @Test
    public void systemClassLoaderExtension() throws NoSuchMethodException {
        System.out.println("system ClassLoader extension");
        ClassIntrospector from = ClassIntrospector.fromCurrent()
            .add(new StringExtension());
        Object myString = from.newInstance("java.lang.String", "hello world")
            .then("initialToUpperCase")
            .getMyThis();
        assertEquals("Hello world", myString);
    }

    @Test
    public void extensionTest2() throws NoSuchMethodException {
        System.out.println("extension 2");
        Object result = new ObjectIntrospector("hello World")
                .add(new StringExtension())
                .then("initialToUpperCase")
                .then("substring", 1, 4)
                .getMyThis();
        assertEquals("ell", result);
    }

    @Test
    public void classExtensionTest() throws MalformedURLException, NoSuchMethodException {
        System.out.println("class extensions");
        ClassIntrospector from = ClassIntrospector.from("src/test/tmp");
        ObjectIntrospector multiplier = from.newInstance("fr.upjv.mis.Multiplier");
        from = from.add((multiplier.getMyThis()));
        Object quaranteDeux = from
                .newInstance("fr.upjv.mis.Dumb2")
                .then("setValue", 10)
                .then("add", 1)
                .then("multiply", 2)
                .then("add", 20)
                .then("getValue")
                .getMyThis();
        assertEquals(42, quaranteDeux);
    }


    @Test
    public void extensionOnNullTest() throws NoSuchMethodException {
        System.out.println("extension with null");
        Object result = new ObjectIntrospector(null)
                .add(new StringExtension())
                .then("initialToUpperCase")
                .getMyThis();
        assertEquals("Hello world", result);
    }


    @Test
    public void extensionDisallowNullTest() throws NoSuchMethodException {
        System.out.println("extension with null");
        System.setProperty("introspection.extensions.disallowNull", "true");
        Object result = new ObjectIntrospector(null)
                .add(new StringExtension())
                .then("initialToUpperCase")
                .getMyThis();
        System.setProperty("introspection.extensions.disallowNull", "false");
        assertEquals(null, result);
    }


    @Test
    public void extensionOnNullTest2() throws NoSuchMethodException, MalformedURLException {
        System.out.println("extension with null 2");
        ClassIntrospector from = ClassIntrospector.from("src/test/resources");
        ObjectIntrospector multiplier = from.newInstance("fr.upjv.mis.Multiplier");
        assertThrows(java.lang.NoSuchMethodException.class,
           () -> new ObjectIntrospector(null)
                .add(multiplier)
                .then("setValue", 10)
                .then("add", 1)
                .then("multiply", 2)
                .then("add", 20)
                .then("getValue")
                .getMyThis()
        );
    }

    @Test
    public void priorityTest() throws NoSuchMethodException {
        System.out.println("priority extension ");
        System.setProperty("introspection.extensions.priority", "true");
        String  value = "hello World";
        Object result = new ObjectIntrospector(value)
                .add(new StringExtension())
                .then("toUpperCase")
                .getMyThis();
        assertEquals(value, result);
        System.setProperty("introspection.extensions.priority", "false");
        result = new ObjectIntrospector(value)
                .add(new StringExtension())
                .then("toUpperCase")
                .getMyThis();
        assertEquals(value.toUpperCase(), result);
        
    }

    @Test
    public void syntheticVarargTest() throws NoSuchMethodException {
        System.out.println("synthetic vararg extension");
        String value = "hello world";
        Object result = new ObjectIntrospector(value)
                .add(new StringExtension())
                .then("nToUpperCase", 0, 2, 4, 6)
                .getMyThis();
        assertEquals("HeLlO World", result);
    }


    @Test
    public void disableSyntheticVarargTest() throws NoSuchMethodException {
        System.out.println("disable synthetic vararg extension");
        System.setProperty("introspection.syntheticVararg", "false");
        String value = "hello world";
        assertThrows(NoSuchMethodException.class, () ->
                new ObjectIntrospector(value)
                .add(new StringExtension())
                .then("nToUpperCase", 0, 2, 4, 6)
                .getMyThis());
               
        System.setProperty("introspection.syntheticVararg", "true");
    }


}

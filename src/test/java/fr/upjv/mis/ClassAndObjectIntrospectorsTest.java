package fr.upjv.mis;


import java.net.MalformedURLException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


/**
 *
 * @author jldeleage
 */
public class ClassAndObjectIntrospectorsTest {
    
    public ClassAndObjectIntrospectorsTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }



    /**
     * Test of get method, of class Introspector.
     */
    @Test
    public void testGetMyThis() throws Exception {
        System.out.println("getMyThis");
        final String    string = "Hello World";
        ObjectIntrospector introspector = new ObjectIntrospector("Hello World");
        assertEquals(string, introspector.getMyThis());
    }

    @Test
    public void testToUpperCase() throws Exception {
        System.out.println("uppercase");
        Object result = new ObjectIntrospector("Hello World")
                .then("toUpperCase")
                .getMyThis();
        assertEquals("HELLO WORLD", result);
    }

    @Test
    public void testSubstring() throws Exception {
        System.out.println("substring");
        Object result = new ObjectIntrospector("Hello World")
                .then("substring", 1, 4)
                .getMyThis();
        assertEquals("ell", result);
    }


    @Test
    public void testChainedCalls() throws Exception {
        System.out.println("chained calls");
        Object result = new ObjectIntrospector("Hello World")
                .then("substring", 1, 4)
                .then("toUpperCase")
                .getMyThis();
        assertEquals("ELL", result);
    }



    @Test
    public void testWithNull() throws Exception {
        System.out.println("with null");
        Object result = new ObjectIntrospector(null)
                .then("substring", 1, 4)
                .then("toUpperCase")
                .getMyThis();
        assertEquals(null, result);
    }


    @Test
    public void testUnknownMethod() throws Exception {
        System.out.println("unknown method");
        assertThrows(NoSuchMethodException.class,
            () -> new ObjectIntrospector("Hello World !")
                .then("substring", 1, 4)
                .then("initialToUpperCase")
                .getMyThis());
    }

    @Test
    public void testUnknownMethod2() throws Exception {
        System.out.println("unknown method 2");
        System.setProperty("introspection.null.isNoSuchMethod", "false");
        Object result = new ObjectIntrospector("Hello World !")
                .then("substring", 1, 4)
                .then("initialToUpperCase")
                .getMyThis();
        System.setProperty("introspection.null.isNoSuchMethod", "true");
        assertEquals(null, result);
    }




    @Test
    public void testDumb() throws Exception {
        System.out.println("dumb");
        Object result = new ObjectIntrospector(new Dumb(10))
                .then("mult", 2)
                .then("add", 1)
                .then("mult", 2)
                .then("getValue")
                .getMyThis();
        assertEquals(42, result);
    }


    @Test
    public void testDumb2() throws Exception {
        System.out.println("dumb 2");
        Object result = new ObjectIntrospector(new Dumb(10))
                .then("mult", 2)
                .then("add", 1)
                .then("mult", 2)
                .then("getValue")
                .getMyThis();
        assertEquals(42, result);
    }


    /**
     * Test newInstance and void method.<br>
     * Actually, runs the code:<br>
     * <pre><code>
     * Introspector.<b>newInstance</b>("fr.upjv.mis.Dumb")
     *           .then(<b>"setValue"</b>, 10)
     *           .then("add", 1)
     *           .then("mult", 2)
     *           .then("add", 20)
     *           .then("getValue")
     *           .get();</code></pre><br>
     * 
     * 
     * @throws Exception 
     */
    @Test
    public void testNewInstance() throws Exception {
        System.out.println("new instance");
        Object quaranteDeux = ClassIntrospector.from("src/test/tmp")
                .newInstance("fr.upjv.mis.Dumb2")
                .then("setValue", 10)
                .then("add", 1)
                .then("mult", 2)
                .then("add", 20)
                .then("getValue")
                .getMyThis();
        assertEquals(42, quaranteDeux);
    }

    @Test
    public void testNewInstanceWithParameter() throws Exception {
        System.out.println("new instance with parameter");
        Object fortyTwo = ClassIntrospector.fromCurrent()
                .newInstance("fr.upjv.mis.Dumb", 10)
                .then("add", 1)
                .then("mult", 2)
                .then("add", 20)
                .then("getValue")
                .getMyThis();
        assertEquals(42, fortyTwo);
    }

    @Test
    public void testOtherClassLoader() throws Exception {
        System.out.println("other ClassLoader");
        Object fortyTwo = ClassIntrospector.from("src/test/")
                .newInstance("fr.upjv.mis.Dumb", 10)
                .then("add", 1)
                .then("mult", 2)
                .then("add", 20)
                .then("getValue")
                .getMyThis();
        assertEquals(42, fortyTwo);
    }

    @Test
    public void testVararg() throws NoSuchMethodException {
        System.out.println("vararg");
        Object fortyTwo = 
                new ObjectIntrospector(new Dumb(5))
                .then("add", new Integer[] {1, 2, 3})
                .then("mult", 2)
                .then("add", 20)
                .then("getValue")
                .getMyThis();
        assertEquals(42, fortyTwo);
    }


    @Test
    public void testPrimitiveVararg() throws NoSuchMethodException {
        System.out.println("primitive vararg");
        Object fortyTwo = 
                new ObjectIntrospector(new Dumb(5))
                .then("add2", 1, 2, 3)
                .then("mult", 2)
                .then("add", 20)
                .then("getValue")
                .getMyThis();
        assertEquals(42, fortyTwo);
    }


    @Test
    public void varargConstructorTest() throws NoSuchMethodException {
        System.out.println("vararg constructor");
        Object fortyTwo = 
                new ObjectIntrospector(new Dumb(3,7))
                .then("add2", 1, 2, 3)
                .then("mult", 2)
                .then("add", 20)
                .then("getValue")
                .getMyThis();
        assertEquals(42, fortyTwo);
    }


    @Test
    public void varargExtensionTest() throws NoSuchMethodException {
        System.out.println("vararg extension");
        Object hElLo = 
                new ObjectIntrospector("hello world")
                .add(new StringExtension())
                .then("camel", 1, 3, 5)
                .getMyThis();
        System.out.println(hElLo);
        assertEquals("hElLo world", hElLo);
    }

    @Test
    public void implicitVarargTest() throws NoSuchMethodException {
        System.out.println("implicit vararg");
        Object result = new ObjectIntrospector(new Dumb(7))
                .then("mult", 1, 2, 3)
                .then("getValue")
                .getMyThis();
        assertEquals(42, result);
    }


    @Test
    public void externalTest() throws NoSuchMethodException, MalformedURLException {
        System.out.println("external");
        ClassIntrospector   cl = ClassIntrospector.from("src/test/tmp");
        Object result = cl.newInstance("fr.upjv.mis.External")
                .then("setExternalValue", 40)
                .then("add", 2)
                .then("getExternalValue")
                .getMyThis();
        System.out.println(result);
    }

    @Test
    public void internalTest() throws NoSuchMethodException, MalformedURLException {
        System.out.println("internal");
        ClassIntrospector   cl = ClassIntrospector.from("src/test/tmp");
        Object result = cl.newInstance("fr.upjv.mis.External")
                .then("getMyInternal")
                .then("setInternalValue", 40)
                .then("add", 2)
                .then("getInternalValue")
                .getMyThis();
        System.out.println(result);
    }


    @Test
    public void useExternalTest() throws NoSuchMethodException, MalformedURLException {
        System.out.println("use external");
        ClassIntrospector   cl = ClassIntrospector.from("src/test/tmp");
        Object external = cl.newInstance("fr.upjv.mis.External")
                .then("setExternalValue", 40)
                .getMyThis();
        Object result = cl.newInstance("fr.upjv.mis.ExternalUser")
                .then("addTo", external, 2)
                .then("getExternalValue")
                .getMyThis()
                ;
        assertEquals(42, result);
    }


    /**
     * We use a ClassLoader to load a class embedding an inner class from a
     * place outside of our classpath.<br>
     * We then load another class using the same ClassLoader to use the latters.
     * 
     * @throws MalformedURLException
     * @throws NoSuchMethodException 
     */
    @Test
    public void useInternalTest() throws MalformedURLException, NoSuchMethodException {
        System.out.println("use internal");
        ClassIntrospector   cl = ClassIntrospector.from("src/test/tmp");
        Object internal = cl.newInstance("fr.upjv.mis.External")
                .then("getMyInternal")
                .then("setInternalValue", 40)
                .getMyThis();
        Object result = cl.newInstance("fr.upjv.mis.ExternalUser")
                .then("addTo", internal, 2)
                .then("getMyInternal")
                .then("getInternalValue")
                .getMyThis()
                ;
        assertEquals(42, result);
    }



    /**
     * Same test as useInternalTest but using two different ClassLoaders.<br>
     * Although these ClassLoaders point to the same URL, the parameter type
     * is not recognized.
     * 
     * @throws NoSuchMethodException
     * @throws MalformedURLException 
     */
    @Test
    public void useDifferentClassLoadersTest() throws NoSuchMethodException, MalformedURLException {
        System.out.println("different class loaders");
        ClassIntrospector   cl1 = ClassIntrospector.from("src/test/tmp");
        Object internal = cl1.newInstance("fr.upjv.mis.External")
                .then("getMyInternal")
                .then("setInternalValue", 40)
                .getMyThis();
        ClassIntrospector   cl2 = ClassIntrospector.from("src/test/tmp");
        assertThrows(NoSuchMethodException.class,
            () -> cl2.newInstance("fr.upjv.mis.ExternalUser")
                .then("addTo", internal, 2)
                .then("getMyInternal")
                .then("getInternalValue")
                .getMyThis()
        );
    }


}       // IntrospectorTest



class Dumb {

    public Dumb() {
    }

    public Dumb(int inValue) {
        value = inValue;
    }

    public Dumb(int... inValues) {
        for (int i=0 ; i<inValues.length ; i++) {
            value += inValues[i];
        }
        value = value/inValues.length;
    }

    public Dumb add(int inValue) {
        Dumb result = new Dumb(value);
        result.value += inValue;
        return result;
    }

    public Dumb add(Integer... inValues) {
        Dumb result = new Dumb(value);
        for (int i=0 ; i<inValues.length ; i++) {
            result.value += inValues[i];
        }
        return result;
    }

    public Dumb add2(int... inValues) {
        Dumb result = new Dumb(value);
        for (int i=0 ; i<inValues.length ; i++) {
            result.value += inValues[i];
        }
        return result;
    }

    public Dumb mult(int inValue) {
        Dumb result = new Dumb(value * inValue);
        return result;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int inValue) {
        value = inValue;
    }

    private     int     value;

}

package fr.upjv.mis;


/**
 *
 * @author jldeleage
 */
public class StringExtension {


    public String nToUpperCase(String inThis, int inIndex) {
        if (inThis == null) {
            return null;
        }
        if (inIndex >= inThis.length()) {
            return inThis;
        }
        return inThis.substring(0, inIndex)
                + inThis.substring(inIndex, inIndex+1).toUpperCase()
                + inThis.substring(inIndex+1);
    }


    public String initialToUpperCase(String inThis) {
        if (inThis == null) {
            inThis = "hello world";
        }
        if (inThis.length() == 0) {
            return inThis;
        }
        return inThis.substring(0,1).toUpperCase() + inThis.substring(1);
    }


    public String camel(String inThis, int... indexes) {
        if (inThis == null) {
            inThis = "hello world";
        }
        StringBuilder   builder = new StringBuilder(inThis);
        for (int i=0 ; i<indexes.length ; i++) {
            char[]      single = new char[1];
            builder.getChars(indexes[i], indexes[i]+1, single, 0);
            String string = new String(single);
            String upper = string.toUpperCase();
            builder.setCharAt(indexes[i], upper.charAt(0));
        }
        return builder.toString();
    }


    public String toUpperCase(String inThis) {
        return inThis;
    }


}

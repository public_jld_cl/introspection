/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.upjv.mis;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 *
 * @author jldeleage
 */
public class IntrospectorHelper {
    

    /**
     * Builds the actual parameters for a vararg method
     * 
     * @param inParameters
     * @param inParameterTypes
     * @return 
     */
    public static Object[]  getParametersWithVararg(Object[] inParameters, Class<?>[] inParameterTypes) {
        if (inParameters.length < inParameterTypes.length) {
            return null;
        }
        int     nbParameterTypes = inParameterTypes.length;
        int     nbNormalParameters = nbParameterTypes - 1;
        // Prepare the result and copy the "normal" parameters
        Object[]    actualParameters = new Object[nbParameterTypes];
        System.arraycopy(inParameters, 0, actualParameters, 0, nbNormalParameters);

        // Let's get the last parameter type
        Class<?>    lastParameterType = inParameterTypes[nbNormalParameters];

        // Get the type of the vararg
        Class<?> componentType = lastParameterType.getComponentType();
        int nbParameters = inParameters.length; 
        int nbRemainingParameters = nbParameters - nbNormalParameters;

        Object remainingParameters = Array.newInstance(componentType, nbRemainingParameters);
        for (int i=0, j=nbNormalParameters ; i<nbRemainingParameters ; i++, j++) {
            arraySet(componentType, remainingParameters, i, inParameters[j]);
        }

        actualParameters[nbNormalParameters] = remainingParameters;

        return actualParameters;
    }       // getParametersWithVararg


    /** Affecte un élément dans l'index du tableau, selon son type */
    private static void arraySet(Class<?> componentType, Object array,
			int index, Object value) {
            if (componentType.isPrimitive()) {
                if (componentType == boolean.class)
                    Array.setBoolean(array, index, ((Boolean) value).booleanValue());
                else if (componentType == byte.class)
                    Array.setByte(array, index, ((Byte) value).byteValue());
                else if (componentType == char.class)
                    Array.setChar(array, index, ((Character) value).charValue());
                else if (componentType == short.class)
                    Array.setShort(array, index, ((Short) value).shortValue());
                else if (componentType == int.class)
                    Array.setInt(array, index, ((Integer) value).intValue());
                else if (componentType == long.class)
                    Array.setLong(array, index, ((Long) value).longValue());
                else if (componentType == float.class)
                    Array.setFloat(array, index, ((Float) value).floatValue());
                else if (componentType == double.class)
                    Array.setDouble(array, index, ((Double) value).doubleValue());
                else
                    throw new IllegalStateException();
            } else {
                Array.set(array, index, value);
            }
	}

}

package fr.upjv.mis;


import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 * Loads a class and instantiate it.<br>
 *
 * @author jldeleage
 */
public class ClassIntrospector extends Introspector {


    private ClassIntrospector(ClassLoader myClassLoader) {
        this.myClassLoader = myClassLoader;
    }


    private ClassIntrospector(ClassIntrospector inParent) {
        super(inParent);
        this.myClassLoader = inParent.myClassLoader;
    }


    public static ClassIntrospector fromCurrent() {
        ClassLoader classLoader = ClassIntrospector.class.getClassLoader();
        return new ClassIntrospector(classLoader);
    }


    public static ClassIntrospector from(String inUrl) throws MalformedURLException {
        URL url;
        try {
            url = new URL(inUrl);
        } catch (MalformedURLException ex) {
            url = new File(inUrl).toURI().toURL();
        }
        return from(url);
    }


    public static ClassIntrospector from(URL inUrl) {
        ClassLoader urlClassLoader = new URLClassLoader(new URL[] { inUrl }, ClassIntrospector.class.getClassLoader());
        return new ClassIntrospector(urlClassLoader);
    }

    @Override
    public ClassIntrospector add(Object... inExtensions) {
        super.add(inExtensions);
        return this;
    }


    public ObjectIntrospector newInstance(String inClassName, Object... inParameters) {
        Class<?> aClass;
        try {
           aClass = myClassLoader.loadClass(inClassName);
        } catch (ClassNotFoundException ex) {
            return new ObjectIntrospector(null, this);
        }
        Constructor<?>[] constructors = aClass.getConstructors();
        // First let's look for a no varargs constructor
        for (Constructor aConstructor : constructors) {
            try {
                Object newInstance = aConstructor.newInstance(inParameters);
                return new ObjectIntrospector(newInstance, this);
            } catch (InstantiationException | IllegalAccessException
                    | IllegalArgumentException | InvocationTargetException ex) {
                continue;
            }
        }   // no vararg

        // Let's try to find a vararg constructor
        for (Constructor aConstructor : constructors) {
            try {
                if (!aConstructor.isVarArgs()) {
                    continue;
                }
                Object[] parametersWithVararg = IntrospectorHelper.getParametersWithVararg(inParameters, aConstructor.getParameterTypes());
                if (parametersWithVararg == null) {
                    continue;
                }
                Object newInstance = aConstructor.newInstance(parametersWithVararg);
                return new ObjectIntrospector(newInstance, this);
            } catch (InstantiationException | IllegalAccessException
                    | IllegalArgumentException | InvocationTargetException ex) {
                continue;
            }
        }   // no vararg

        return new ObjectIntrospector(null, this);
    }

    public ClassLoader getMyClassLoader() {
        return myClassLoader;
    }

    public Class loadClass(String inName) throws ClassNotFoundException {
        return myClassLoader.loadClass(inName);
    }


    ClassLoader     myClassLoader;


}

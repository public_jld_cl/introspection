package fr.upjv.mis;


import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;



/**
 *
 * @author jldeleage
 */
public abstract class Introspector {


    public Introspector() {
        
    }


    public Introspector(Introspector inParent) {
        if (inParent.extensions != null) {
            extensions = new LinkedList<>();
            extensions.addAll(inParent.extensions);
        }
    }


    protected List<Object>   getExtensions() {
        if (extensions == null) {
            return Collections.EMPTY_LIST;
        } else {
            return extensions;
        }
    }


    public Introspector add(Object... inExtensions) {
        if (extensions == null) {
            extensions = new LinkedList<>();
        }
        extensions.addAll(Arrays.asList(inExtensions));
        return this;
    }


    private     List<Object> extensions;


}

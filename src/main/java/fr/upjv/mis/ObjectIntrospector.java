package fr.upjv.mis;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static jdk.internal.org.jline.keymap.KeyMap.key;



/**
 *
 * @author jldeleage
 */
public class ObjectIntrospector extends Introspector {


    public ObjectIntrospector(Object myThis) {
        this.myThis = myThis;
    }


    public ObjectIntrospector(Object inMyThis, Introspector inParent) {
        super(inParent);
        myThis = inMyThis;
    }


    public ObjectIntrospector add(Object... inExtensions) {
        super.add(inExtensions);
        return this;
    }


    public ObjectIntrospector then(String inMethodName, Object... inParameters) throws NoSuchMethodException {
        ObjectIntrospector result;
        if (isExtensionPriority()) {
            result = invokeExtension(inMethodName, inParameters);
            if (result == null) {
                result = invokeNormalMethod(inMethodName, inParameters);
            }
        } else {
            result = invokeNormalMethod(inMethodName, inParameters);
            if (result == null) {
                result = invokeExtension(inMethodName, inParameters);
            }
        }
        if (result != null) {
            return result;
        }

        if (isNullNoSuchMethod()) {
            throw new NoSuchMethodException(inMethodName);
        } else {
            return EMPTY;
        }
    }

    protected ObjectIntrospector invokeNormalMethod(String inMethodName, Object... inParameters) throws NoSuchMethodException {
        if (myThis != null) {
            Class<? extends Object> aClass = myThis.getClass();
            Method[] methods = aClass.getMethods();
            // Look for a "normal" method
            for (Method aMethod : methods) {
                if (! aMethod.getName().equals(inMethodName)) {
                    continue;
                }
                Object result;
                try {
                    result = aMethod.invoke(myThis, inParameters);
                    return wrap(aMethod, result);
                } catch (IllegalAccessException | IllegalArgumentException ex) {
                    continue;
                } catch (InvocationTargetException ex) {
                    internalException = ex.getCause();
                    continue;
                }
            }

            // Look for a vararg method
            for (Method aMethod : methods) {
                if (! aMethod.getName().equals(inMethodName)) {
                    continue;
                }
                if (! aMethod.isVarArgs()) {
                    continue;
                }
                Object[] parametersWithVararg = IntrospectorHelper.getParametersWithVararg(inParameters, aMethod.getParameterTypes());
                if (parametersWithVararg == null) {
                    continue;
                }
                try {
                    Object result = aMethod.invoke(myThis, parametersWithVararg);
                    return wrap(aMethod, result);
                } catch (IllegalAccessException | IllegalArgumentException ex) {
                    continue;
                } catch (InvocationTargetException ex) {
                    internalException = ex.getCause();
                    continue;
                }
            }       // look for vararg method

            // look for a "synthetic vararg" method
            String allowSyntheticVararg = System.getProperty("introspection.syntheticVararg", "true");
            if ("true".equals(allowSyntheticVararg)) {
                for (Method aMethod : methods) {
                    if (! aMethod.getName().equals(inMethodName)) {
                        continue;
                    }
                    Class<?>[] parameterTypes = aMethod.getParameterTypes();
                    int     nbParameterTypes = parameterTypes.length;
                    if (inParameters.length <= nbParameterTypes) {
                        continue;
                    }
                    int     nbNormalParameters = nbParameterTypes - 1;
                    // Prepare the result and copy the "normal" parameters
                    Object[]    actualParameters = new Object[nbParameterTypes];
                    System.arraycopy(inParameters, 0, actualParameters, 0, nbNormalParameters);
                    Object result = myThis;
                    try {                    
                        for (int i=nbNormalParameters ; i<inParameters.length ; i++) {
                            actualParameters[nbNormalParameters] = inParameters[i];
                            myThis = aMethod.invoke(myThis, actualParameters);
                        }
                        return wrap(aMethod, myThis);
                    } catch (Exception ex) {
                        myThis = result;
                    }
                }
            }

        }       // myThis != null
        return null;
    }

    protected ObjectIntrospector invokeExtension (String inMethodName, Object... inParameters) throws NoSuchMethodException {
        // Do we allow to run an extension on a void reference ?
        if (myThis == null && isNullDisallowed()) {
            return this;
        }
        // Look for a "normal" extension
        List<Object> extensions = getExtensions();
        Object[] params = new Object[inParameters.length + 1];
        params[0] = myThis;
        System.arraycopy(inParameters, 0, params, 1, inParameters.length);
        for (Object anExtension : extensions) {
            Class    extensionClass = anExtension.getClass();
            Method[] methods = extensionClass.getMethods();
            for (Method aMethod : methods) {
                Object result;
                if (!aMethod.getName().equals(inMethodName)) {
                    continue;
                }
                try {
                    result = aMethod.invoke(anExtension, params);
                    return wrap(aMethod, result);
                } catch (IllegalAccessException | IllegalArgumentException ex) {
                    continue;
                } catch (InvocationTargetException ex) {
                    internalException = ex;
                    continue;
                }
            }
        }

        // Look for a vararg extension
        for (Object anExtension : extensions) {
            Class    extensionClass = anExtension.getClass();
            Method[] methods = extensionClass.getMethods();
            for (Method aMethod : methods) {
                Object result;
                if (!aMethod.getName().equals(inMethodName)) {
                    continue;
                }
                try {
                    Object[] parametersWithVararg = IntrospectorHelper.getParametersWithVararg(params, aMethod.getParameterTypes());
                    result = aMethod.invoke(anExtension, parametersWithVararg);
                    return wrap(aMethod, result);
                } catch (InvocationTargetException ex) {
                    internalException = ex;
                    continue;
                } catch (Exception ex) {
                    continue;
                }
            }
        }

        // look for a "synthetic vararg" extension
        String allowSyntheticVararg = System.getProperty("introspection.syntheticVararg", "true");
        if ("true".equals(allowSyntheticVararg)) {
            for (Object anExtension : extensions) {
                Class    extensionClass = anExtension.getClass();
                Method[] methods = extensionClass.getMethods();
                for (Method aMethod : methods) {
                    if (! aMethod.getName().equals(inMethodName)) {
                        continue;
                    }
                    Class<?>[] parameterTypes = aMethod.getParameterTypes();
                    int     nbParameterTypes = parameterTypes.length;
                    if (inParameters.length <= nbParameterTypes) {
                        continue;
                    }
                    int     nbNormalParameters = nbParameterTypes - 1;
                    // Prepare the result and copy the "normal" parameters
                    Object[]    actualParameters = new Object[nbParameterTypes];
                    actualParameters[0] = myThis;
                    Object result = myThis;
    //                if (nbNormalParameters > 0) {
                        System.arraycopy(inParameters, 0, actualParameters, 1, nbNormalParameters-1);
    //                }
                    try {
                        for (int i=nbNormalParameters-1 ; i<inParameters.length ; i++) {
                            actualParameters[0] = result;
                            actualParameters[nbNormalParameters] = inParameters[i];
                            result = aMethod.invoke(anExtension, actualParameters);
                        }
                        return wrap(aMethod, result);
                    } catch (Exception ex) {
                        myThis = result;
                    }
                }
            }
        }

        if (myThis == null) {
            return this;
        }
        return null;
    }


    //========================================================================//


    public Object getMyThis() {
        return myThis;
    }


    //========================================================================//
    //  P A R A M E T E R S
    //========================================================================//


    protected boolean isExtensionPriority() {
        String priority = System.getProperty("introspection.extensions.priority", "false");
        return ("true".equals(priority));
    }

    protected boolean isNullDisallowed() {
        String disallowed = System.getProperty("introspection.extensions.disallowNull", "false");
        return "true".equals(disallowed);
    }


    protected boolean isNullNoSuchMethod() {
        String disallowed = System.getProperty("introspection.null.isNoSuchMethod", "true");
        return "true".equals(disallowed);
    }


    //========================================================================//
    //  U T I L I T Y
    //========================================================================//


    private ObjectIntrospector wrap(Method aMethod, Object aValue) {
        if (aValue == null) {
            if (aMethod.getReturnType().getName().equals("void"))
               return new ObjectIntrospector(myThis, this);
            else 
               return EMPTY;
        }
        return new ObjectIntrospector(aValue, this);
    }


    //========================================================================//


    private              Object             myThis;
    private              Throwable          internalException;
    private final static ObjectIntrospector EMPTY = new ObjectIntrospector(null);

}

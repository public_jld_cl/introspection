# Introspection v. 3.2.1

A very small library to make introspection easy.

Typical uses:

```
Object quaranteDeux = ClassIntrospector.from("target/generated-test-classes")
    .newInstance("fr.upjv.mis.Dumb")
    .then("setValue", 10)
    .then("add", 1)
    .then("mult", 2)
    .then("add", 20)
    .then("getValue")
    .getMyThis();
assertEquals(42, quaranteDeux);
```

```
Object fortyTwo = new ObjectIntrospector(myObjectInputStream.readObject())
    .then("add", 1)
    .then("mult", 2)
    .then("add", 20)
    .then("getValue")
    . getMyThis();
assertEquals(42, fortyTwo);
```

```
String myString = new ObjectIntrospector("hello world")
	.add(new StringExtension())
	.then("initialToUpperCase")
	.getMyThis();
assertEquals("Hello world", myString);
```

## Rationale

The goal is to **make introspection easy to use**.
Main use cases are:

- test of generated classes ; in an IDE, it is not always easy to write such tests
- use of dynamically loaded classes or serialized objects

By the way, the library adds *extensions* to the introspected objects.

## Prerequisites

The library is packaged as a Maven project (Java 8).
In order to build the library from source, please install Maven on you computer.

## Features Summary

As we can see in the previous examples, the `Introspector` class allows to chain calls.

The examples demonstrate some features:

* simplified URLClassLoader (the `from` static method)
* dynamic instanciation (the `newInstance` method)
* overloaded methods and constructors (the `Dumb` class owns two constructors, one with no parameter, one with one parameter)
* `void` method (the `setValue` method returns `void` but it is possible to continue invoking method of the same `Dumb` instance)
* *extensions* (the `initialToUpperCase` has been *added* to the `String`class)

Furthermore, `null` value is gracefuly handled and a new notion, *synthetic varargs*, has been added.

## Simplified URLClassLoader

To load a class from any URL we first instantiate `ClassIntrospector` using the static `from` method:

```
ClassIntrospector classIntrospector = ClassIntrospector.from("target/generated-classes");
```

When no protocol is provided as in this example, the URL is assumed to be a path in the file system.

The static `fromCurrent` method returns a `ClassIntrospector` instance using the application classpath.

## Simplified Instantiation

To instantiate a class, we use the `newInstance` method of `ClassIntrospector`. This method takes the fully qualified identifier of the class as parameter.

The `newIntance` method can use any public constructor of the class, including a *vararg* constructor (reflective invocation of a *vararg* method can be tricky when dealing with primitive parameters).

The library looks for a constructor matching the parameters.

The method returns an instance of `ObjectIntrospector` wrapping the new instance:

```
ObjectIntrospector objIntrospector = classIntrospector.newInstance("fr.upjv.mis.Dumb");
```

It is possible to wrap any object into an instance of `ObjectIntrospector`:

```
ObjectIntrospector objIntrospector =  new ObjectIntrospector(myObject);
```

## Simplified Invocation

To invoke a method we use the `then` method of the class `ObjectIntrospector`. 
This method takes the name of the method to invoke as first parameter and a *vararg* for the parameters of the invocation.
The `then` method returns a fresh instance of `ObjectIntrospector`, so it is easy to chain invocations.

```
ObjectIntrospector objIntrospector
    = classIntrospector.newInstance("fr.upjv.mis.Dumb")
    .then("setValue", 20)
    .then("add", 1);
```

*vararg* methods are supported.


## Getting the wrapped object

The `getMyThis` method returns the wrapped object:

```
Object twenty
    = classIntrospector.newInstance("fr.upjv.mis.Dumb", 20)
    .then("getValue")
    .getMyThis();
assertEquals(20, twenty);
```


## `void` Result

In case a method returns no result (`void` method), the `then` method  returns a new instance of `ObjectIntrospector` wrapping the same object.
In the following example, the `setValue` method returns `void` but it is possible to chain an invocation on the same target:

```
Object obj
    = classIntrospector.newInstance("fr.upjv.mis.Dumb")
    .then("setValue", 20)			// setValue returns void
    .then("add", 1)                 // this is fine
    .then("mult", 2)
    .getMyThis();
```


## `null` Handling

When an `ObjectIntrospector`wraps a `null` value, all subsequent method invocations return a new `ObjectIntrospector` wrapping a `null` reference.
Such instance of `ObjectIntrospector` can execute any method as in the *null object pattern* (https://en.wikipedia.org/wiki/Null_object_pattern):

```
Object obj = 
    new ObjectIntrospector(null)
    .then("setValue", 20)				// this is fine
    .then("add", 1)                     // this is fine too
    .then("mult", 2)
    .getMyThis();		                // returns null
```

In a way, `ObjectIntrospector` has the same behaviour than `Optional`


## *Synthetic vararg*

The following code works, although there is no actual `mult` matching method:

```
Object fortyTwo = ClassIntrospector.from("target/generated-test-classes")
    .newInstance("fr.upjv.mis.Dumb")
    .then("setValue", 7)
    .then("mult", 1, 2, 3)
    .then("getValue")
    .getMyThis();
assertEquals(42, fortyTwo);
```

The `Dumb` class contains the method `public Dumb mult(int inValue)`.

Actually, when the `ObjectIntrospector` tries to run the `mult` method, it runs that method three times, with 1, 2 and 3 as respective parameters.

This way the last parameter of every method is treated as if it were automatically turned into a vararg.

This feature can be disabled.



## *Extensions*

`ClassIntrospector` and `ObjectIntrospector` accept *extensions*.
An *extension* adds methods to existing objects ([https://en.wikipedia.org/wiki/Extension_method](https://en.wikipedia.org/wiki/Extension_method))

To add an *extension*, simply use the `add` method of a `ClassIntrospector` or `ObjectIntrospector` instance:

```
String myString = new ObjectIntrospector("hello world")
	.add(new StringExtension())
	.then("initialToUpperCase")
	.getMyThis();
assertEquals("Hello world", myString);
```

In this example, the `StringExtension` class contains the method:

```
public String initialToUpperCase(String inThis) {
    return inThis.substring(0,1).toUpperCase() + inThis.substring(1);
}
```

The first argument of the *extension* method is the object wrapped in the `ObjectIntrospector`instance.

Note: by default *extensions* have a lower priority than *normal* methods but this can be modified using a System property (see below).

Note: if you are interested in *extensions* in Java, take a look at [Manifold](http://manifold.systems) for example.

## *Extensions* and local classes

In the previous example, the *extension* was added to an instance.
But it is possible to add *extensions* to all instances of a local class:

```
ClassIntrospector from = ClassIntrospector.fromCurrent()
    .add(new StringExtension());
Object myString = from.newInstance("java.lang.String", "hello world")
    .then("initialToUpperCase")
    .getMyThis();
assertEquals("Hello world", myString);

```

All objects created through the `newInstance` method of the `from` `ClassIntrospector` will be able to run the `StringExtension` methods.

Warning: in this example, *extensions* are added to classes loaded by the `from` object only. If we load the same class
through an other ClassLoader, they won't be added.


## *Extensions* and `null` reference

An *extension* can manage a `null`reference.

For example, let's consider the following *extension* method, in the `StringExtension` class:

```
    public String initialToUpperCase(String inThis) {
        if (inThis == null) {
            inThis = "hello world";
        }
        return inThis.substring(0,1).toUpperCase() + inThis.substring(1);
    }
```

Then the following test runs fine:

```
Object result = new ObjectIntrospector(null)
	.add(new StringExtension())
	.then("initialToUpperCase")
	.getMyThis();
assertEquals(null, result);
```


If we had not handled the `null` reference in the *extension* method, the invocation of `initialToUpperCase` would have thrown a `NoSuchMethod` exception.

Note: *extensions* can be skipped in case of a `null`reference instad of throwing an exception (see below)


## Integration in your projects

The library is supposed to work fine in any Java ≥ 8.

1 Download and build the library:

```
git clone https://gitlab.com/ete-group/introspection.git
cd introspection
mvn clean install
```


2 Add it to the classpath of you project.

```
<dependency>
    <groupId>fr.upjv.mis</groupId>
    <artifactId>introspection</artifactId>
    <version>3.2.1</version>
</dependency>
```

The library is not available on Maven Central yet, you need to clone and build it.


## Parameters

Since version 3.1.1, it is possible to configure some features through System properties

* `introspection.extensions.disallowNull` (default `false`). Prevents *extensions* to be run on `null`reference if set to `true`
* `introspection.extensions.priority` (default `false`). Gives priority to *extensions* methods over *normal* methods if set to `true`
* `introspection.null.isNoSuchMethod` (default `true`). If set to `true`, an instance of `ObjectIntrospector` instance wrapping a `null` reference instead of throwing a `NoSuchMethodException` when no matching method is found, either *normal* or *extension*.
* `introspection.syntheticVararg`(default `true`). Enables/disables *synthetic varargs*.



## Authors
Jean-Luc Déléage (jean-luc@insset.fr) and Christophe Logé (christophe.loge@u-picardie.fr)

## History

1. Very first release
2. Split the code into three classes to simplify API relative to classloading
3. Add *extensions* and *varargs*

## License

This product and its source code is under [Apache 2.0 license](https://www.apache.org/licenses/LICENSE-2.0.txt)

© 2022 JL Déléage - C Logé


